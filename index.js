const fs = require("fs")
const axios = require('axios');
const jp = require('jsonpath');
// TODO a passer en conf
const urlCatalog = "https://federated-catalogue-api.aster-x.demo23.gxfs.fr/query_page?page=0&size=10000"
const url = "http://localhost:8983/solr/my-collection/update?commit=true"

const auhSolr = { username : "admin" , password : process.env.SOLR_PASSWORD } 

let jpathFields = [
    {
        "gx:ServiceOffering" : [
            { "gx_id" : "$.id" },
            { "gx_issuer" : "$.issuer" },
            { "gx_type" : "$.type" },
            { "gx_issuanceDate" : "$.issuanceDate" },
            { "gx_credentialSubject_id" : "$.credentialSubject.id" },
            { "gx_credentialSubject_type" : "$.credentialSubject.type" },
            { "gx_title" : "$.credentialSubject['gx:name']" },
            { "gx_gxkeyword" : "$.credentialSubject['gx:keyword']" },
            { "gx_keyword_str" : "$.credentialSubject['gx:keyword']" },
            { "gx_description" : "$.credentialSubject['gx:description']" },
            { "gx_conformities" : "$.credentialSubject['gx:endpoint']['gx:standardConformity'].*['gx:title']"},
            { "gx_layers" : "$.credentialSubject['aster-conformity:layer']" },
         ]
    }, 
    {
        "aster-conformity:Location" : [
            { "gx_country" :  "$.credentialSubject['aster-conformity:country']" }, 
            { "gx_state" : "$.credentialSubject['aster-conformity:state']" },
            { "gx_urbanArea" : "$.credentialSubject['aster-conformity:urbanArea']" },
            { "gx_administrativeLocation" : "$.credentialSubject['aster-conformity:hasAdministrativeLocation']" }
         ]
    }
]

async function getCatalog () { 
    let cat = await axios.post(urlCatalog,  {  } )
    return cat.data
}


async function indexServicesOffering (catalog) {
    let documents = []

    for await (const aServiceOffering of catalog.results) {
        let fieldServiceOffering = {}
        // types de VC dans le tableau de VC pour un service offering
        let vcTypes = jp.query(aServiceOffering, "$..credentialSubject.type");
        /* pour info liste possible   'gx:ServiceOffering','gx:LegalParticipant','aster-conformity:LocatedServiceOffering','aster-conformity:Location','aster-conformity:grantedLabel'  */
        let fields = {}
        for await (const vcType of vcTypes) {
            let jpathForThisVC = jp.query(jpathFields, '$..["' + vcType + '"]')[0]
            let fieldsExtrac = {}
            let vc = aServiceOffering [vcTypes.indexOf(vcType)] ;
            if (vc==undefined) {
                console.log ("erreur pas trouve le service offering" + vcType)
            }
            if (jpathForThisVC!=undefined && vc!=undefined) {
                fieldsExtrac = await extractFields (vc,jpathForThisVC)
            } 
            fields = Object.assign ( fields , fieldsExtrac)
        }
        fields ['conformitiesList'] = jp.query( aServiceOffering,"$..['aster-conformity:hasComplianceReferenceTitle']")  // 
        fields['geo'] = fields.gx_country + '/' + fields.gx_state + '/' + fields.gx_urbanArea;
        fields['kind'] = "serviceOffering"
        fields['vc'] = JSON.stringify(aServiceOffering)
        documents.push (fields)
    } 
    pushSolr(documents)
}

async function extractFields(vc,jpatharray) {
    let fieldsExtracted = {}
    for await (const field of jpatharray) {
        for await (const key of Object.keys(field)) {
            if (jp.query(vc, field[key])[0] != undefined) {
                let r = jp.query(vc, field[key]);
                if (key=="conformities") {
                    fieldsExtracted [key] =  (r==undefined ? '' : r )
                } else {
                    fieldsExtracted [key] =  (r[0]==undefined ? '' : r )
                }
            }
        }
    }
    return fieldsExtracted;
}

async function pushSolr (fields) {
    let headers= 
    axios.post(url, JSON.stringify(fields), {headers: {'Content-Type': 'application/json'} , auth: {username: auhSolr.username,password: auhSolr.password
  } })
      .then((response) => {
            console.log ("ok")
        })
      .catch((error) => {
        console.log ("error", error)
      })
}

async function main () {
    await indexServicesOffering (await getCatalog())
}
main ()
